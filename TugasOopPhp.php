<?php

class Hewan{
    use Fight;
    public  $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    public function atraksi(){
        echo "$this->nama sedang $this->keahlian";
    }

    abstract public function getInfoHewan();

}

    trait Fight{
    public  $attackPower,
            $defencePower;
    
    public function serang(Hewan $hewan1, Hewan $hewan2){

        echo "$hewan->nama sedang menyerang $hewan->nama \n";
        $this->diserang($hewan2, $hewan1);
    }

    public function diserang(Hewan $hewan2, Hewan $hewan1){
        echo "$hewan->nama sedang diserang $hewan1->nama \n";
    }
}

class Elang extends Fight{
    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "Nama : $this->nama \n jumlah kaki: $this->jumlahKaki \n Keahlian: $this->keahlian \n Attack Power: $this->attackPower \n Defence Power: $this->defencePower";
    }
}

class Harimau extends Hewan{

    public function __construct($name){
        $this->nama = $name;
        $this->jumlahKaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "Nama : $this->nama \n jumlah kaki: $this->jumlahKaki \n Keahlian: $this->keahlian \n Attack Power: $this->attackPower \n Defence Power: $this->defencePower";

}

$elang1 = new Elang("elang1");
$harimau5 = new harimau("harimau5");

$elang1->serang($elang1, $harimau5);
$elang1->getInfoHewan();

// var_dump($elang);
